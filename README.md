# README
This is a school project for the Artificial Intelligence subject.
It implements two different go players :
- one with the alpha-beta algorithm, heuristics, and various strategies (GO directory)
- one with the help of the Keras library (ML diretory)

## Go
For more information, please refer to the `README.md` located in the GO directory.

## ML
For more information, please refer to the `README.md` located in the ML directory.

# Authors
- Sophie STAN (sstan@enseirb-matmeca.fr)
- Antoine PRINGALLE (apringalle@enseirb-matmeca.fr)