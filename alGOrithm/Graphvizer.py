class Graphvizer():
    """
    Class used to display the tree computed by minimax search
    """
    _node = 0

    def __init__(self):
        self._file = open("graph.dot", "w")
        self._file.write("digraph minimax_search {\n")

    def close(self):
        self._file.write("}\n")
        self._file.close()

    def append_node(self):
        self._node += 1
        return self._node

    def next_node(self):
        return "node" + str(self.append_node())

    def write(self, line):
        self._file.write(line)

    def node(self, move):
        node = self.next_node()
        self._file.write(f'\t{node} [shape=square, label="score"];\n')
        return node, move

    def move(self, parent, child):
        self._file.write(f'\t{parent[0]} -> {child[0]} [label="{child[1]}"];\n')
        return child

    def score(self, node, score):
        self._file.write(f'\t{node[0]} [label="{score}"];\n')

    def alpha(self, child):
        self._file.write(f'\t{child[0]} [color="cornflowerblue"];\n')

    def beta(self, child):
        self._file.write(f'\t{child[0]} [color="orange"];\n')
