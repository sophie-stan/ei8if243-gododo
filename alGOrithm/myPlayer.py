# -*- coding: utf-8 -*-
""" The interface of this class is defined in playerInterface.py """

from functools import lru_cache
from time import time

import Goban
from playerInterface import *
import ai


class myPlayer(PlayerInterface):
    """ Example of a random player for the go. The only tricky part is to be able to handle
    the internal representation of moves given by legal_moves() and used by push() and
    to translate them to the GO-move strings "A1", ..., "J8", "PASS". Easy!
    """

    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None
        self._last_opponent_move = None
        self._my_hash = dict()
        self._totalTime = None

    # Returns your player name, as to be displayed during the game
    def getPlayerName(self):
        return "GOdodo Player"

    # Returns your move. The move must be a valid string of coordinates ("A1",
    # "D5", ...) on the grid or the special "PASS" move. a couple of two integers,
    # which are the coordinates of where you want to put your piece on the board.
    # Coordinates are the coordinates given by the Goban.py method legal_moves().
    def getPlayerMove(self):
        begin = time()

        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"

        # Added by Antoine, for debug purposes
        moves = self._board.legal_moves()  # Dont use weak_legal_moves() here!
        print("Referee told me I can play those moves: ", [self._board.move_to_str(m) for m in moves])

        move = ai.AI.get_ai_move(self._board, self._totalTime, self._my_hash)
        self._board.push(move)

        # New here: allows to consider internal representations of moves
        print("I am playing ", self._board.move_to_str(move))
        print("My current board :")
        self._board.prettyPrint()
        # move is an internal representation. To communicate with the interface I need to change it to a string

        self._totalTime += time() - begin
        return Goban.Board.flat_to_name(move)

    def playOpponentMove(self, move):
        print("Opponent played ", move)  # New here
        self._last_opponent_move = move
        self._board.push(Goban.Board.name_to_flat(move))

    # Give your color. As defined in Goban.py : color=1
    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)
        self._totalTime = 0

    def endGame(self, winner: int):
        if self._mycolor == winner:
            print("   _     _       \033[93m___________  \033[0m")
            print("  (o\\---/o)     \033[93m'._==_==_=_.' \033[0m")
            print("   ( . . )      \033[93m.-\\       /-. \033[0m")
            print("  _( (T) )_   /\033[93m| (|  # 1  |) |\033[0m")
            print(" / / ___   \\_/ /\033[93m'-|       |-' \033[0m")
            print("/ / (   ) \\___/\033[93m   \\       /   \033[0m")
            print("\\_)  ¯¯¯   |       \033[93m'.   .'    \033[0m")
            print("  \\   _   /          \033[93m) (      \033[0m")
            print("   )  |  (_        \033[93m_.' '._    \033[0m")
            print(" (___,'.___)      \033[93m|_______|   \033[0m")
        else:
            print("I lost ¯\_(ツ)_/¯")

