"""
Lance une partie de `MAX_MOVES` coups avec des joueurs random. Une fois qu'on
a atteint le tour en question, on lance un minimax sur le plateau. ça crée un
graph pour avoir visuellement ce que fait minimax. Chaque noeud est un plateau,
il est nommé selon le score que lui attribue `heuristique(board)`. Quand il y
a un élagage alpha ou beta, le noeud est coloré (on n'évaluera pas les plateaux
suivants)
"""

import sys
import time
from io import StringIO

import Goban
import Graphvizer
import randomPlayer

MAX_MOVES = 25

#####################################################
#  SAME FUNCTIONS IN MYPLAYER.PY BUT WITH GRAPHVIZ  #
#####################################################

INFINITY = 100000
OPPONENTCOLOR = 2
MYCOLOR = 1


def minimax(board, depth, player, graphviz=False):
    chosen_move = ''
    score_max = -INFINITY
    liste = []  # debug purposes

    graph = Graphvizer.Graphvizer()  # graph of minimax search 
    PREVIOUS = graph.node("start")

    for move in board.legal_moves():
        NEXT = graph.move(PREVIOUS, graph.node(move))

        board.push(move)
        res = alphabeta(board, -INFINITY, INFINITY, depth - 1, player, graph, NEXT)
        graph.score(NEXT, res)
        board.pop()
        if res > score_max:
            score_max = res
            chosen_move = move
    print("liste:", sorted(liste, reverse=True), f"len:{len(liste)}")  # debug purposes
    print("choisi: {} ({})".format(chosen_move, score_max))
    graph.score(PREVIOUS, score_max)
    graph.close()
    print("File written")

    return chosen_move


def alphabeta(board, alpha, beta, depth, player, graph, NEXT):
    """ Minimax with alpha beta pruning
    
    Computes the 'value' of a move.
    
    Arguments:
        board -- The board with the move played
        alpha -- Used for alpha-beta pruning. alpha is initialized to -∞.
        beta -- Used for alpha-beta pruning. alpha is initialized to +∞. Always alpha > beta 
        depth -- depth of the search
        player -- 1 for BLACK and 2 for WHITE. We use min() for oponent and max() for us. 
    
    Returns:
        It returns the 'value' of a move.
    """
    if depth == 0 or board.is_game_over():
        score = heuristic(board)
        graph.score(NEXT, score)
        return score

    elif player == MYCOLOR:
        v = +INFINITY
        PREVIOUS = NEXT
        for move in board.legal_moves():
            NEXT = graph.move(PREVIOUS, graph.node(move))
            board.push(move)
            v = min(v, alphabeta(board, alpha, beta, depth - 1, OPPONENTCOLOR, graph, NEXT))
            board.pop()
            if alpha >= v:  # coupure alpha
                graph.alpha(NEXT)
                print("\033[91mCOUPURE ALPHA\033[0m")
                graph.score(PREVIOUS, v)
                return v
            beta = min(beta, v)

    else:
        v = -INFINITY
        PREVIOUS = NEXT
        for move in board.legal_moves():
            NEXT = graph.move(PREVIOUS, graph.node(move))
            board.push(move)
            v = max(v, alphabeta(board, alpha, beta, depth - 1, MYCOLOR, graph, NEXT))
            board.pop()
            if v >= beta:  # coupure beta
                graph.beta(NEXT)
                print("\033[93mCOUPURE BETA\033[0m")
                graph.score(PREVIOUS, v)
                return v
            alpha = max(alpha, v)

    graph.score(PREVIOUS, v)
    return v


def heuristic(board):
    value = board._nbWHITE - board._nbBLACK + board._capturedWHITE - board._capturedBLACK
    if MYCOLOR == 2:
        return value
    return -value


##########################
#     LAUCHNING GAME     #
##########################
if __name__ == "__main__":
    b = Goban.Board()

    players = []
    player1 = randomPlayer.myPlayer()
    player1.newGame(Goban.Board._BLACK)
    players.append(player1)

    player2 = randomPlayer.myPlayer()
    player2.newGame(Goban.Board._WHITE)
    players.append(player2)

    totalTime = [0, 0]  # total real time for each player
    nextplayer = 0
    nextplayercolor = Goban.Board._BLACK
    nbmoves = 1

    outputs = ["", ""]
    sysstdout = sys.stdout
    stringio = StringIO()
    wrongmovefrom = 0

    while not b.is_game_over() and nbmoves < MAX_MOVES:
        b.prettyPrint()
        legals = b.legal_moves()  # legal moves are given as internal (flat) coordinates, not A1, A2, ...
        nbmoves += 1
        otherplayer = (nextplayer + 1) % 2
        othercolor = Goban.Board.flip(nextplayercolor)

        currentTime = time.time()
        sys.stdout = stringio
        move = players[
            nextplayer].getPlayerMove()  # The move must be given by "A1", ... "J8" string coordinates (not as an
        # internal move)
        sys.stdout = sysstdout
        playeroutput = stringio.getvalue()
        stringio.truncate(0)
        stringio.seek(0)
        outputs[nextplayer] += playeroutput
        totalTime[nextplayer] += time.time() - currentTime

        if not Goban.Board.name_to_flat(move) in legals:
            wrongmovefrom = nextplayercolor
            break
        b.push(Goban.Board.name_to_flat(move))  # Here I have to internally flatten the move to be able to check it.
        players[otherplayer].playOpponentMove(move)

        nextplayer = otherplayer
        nextplayercolor = othercolor

    move = minimax(b, 3, 1, graphviz=True)
