# Machine learning

Le but est d'entraîner un modèle à évaluer des plateaux de go. Nous avions à notre disposition 41563 plateaux avec leur
score. La lecture de ce jupyter Notebook trace l'évolution chronologique de notre travail. Nous avons dupliqué les
données d'exemple, puis nous avons défini notre modèle à tâtons. Nous sommes fiers de vous présenter un **modèle avec
une MAE de 0,0822.**

## Contributeurs

Équipe n°15
- Sophie STAN (sstan@enseirb-matmeca.fr)
- Antoine PRINGALLE (apringalle@enseirb-matmeca.fr)

## Environnement de travail

L'entraînement a été réalisé sur Google Colab, avec l'option GPU pour :

- Ne pas faire souffrir nos petites machines
- gagner beaucoup de temps dans la phase de tests de paramètres a.k.a. la cuisine
- pouvoir monter le nombre d'epochs une fois le modèle assez bien défini
- travailler ensemble à distance

## Récupération des données d'entraînement

On télécharge les données d'entraînement au format json. Il sera utilisé pour créer un tableau Numpy sur lequel nous
ferons notre entraînement.

## Conversion json --> Numpy

Ensuite, nous définissons les fonctions permettant de passer d'un fichier json à un tableau numpy.

Les éléments de ce tableau auront pour shape (2, 9 ,9). Un plateau est donc représenté par **2 tableaux 9x9**. Le
premier indique où se trouvent les pions noirs, le deuxième indique où sont les blancs.

## Multiplication des données pour l'entraînement

Le fichier fourni met à notre disposition 41563 sur lesquels s'entraîner. C'est bien mais on peut faire beaucoup mieux.

Puisque dans le jeu de go, la valeur d'un plateau est insenible à toute rotation ou toute symétrie, on peut générer
d'autres plateaux à partir des exemples d'origine. Nous réalisons :

- les symétries Haut-Bas
- les symétries Gauche-Droite
- les rotations de 90°
- les rotations de 180°
- les rotations de 270°
- la combinaison symétrie Haut-Bas + rotation de 90°
- la combinaison symétrie Haut-Bas + rotation de 180°
- la combinaison symétrie Haut-Bas + rotation de 270°
- la combinaison symétrie Gauche-Droite + rotation de 90°
- la combinaison symétrie Gauche-Droite + rotation de 180°
- la combinaison symétrie Gauche-Droite + rotation de 270°

On a donc une **multiplication de nos données d'origine par 12** ce qui nous donne 498756 données.

## Séparation des données d'entraînement, de test et de validation

On utilise la fonction `train_test_split` de sklearn pour séparer nos données. On lui dit de garder 80% des données pour
l'entraînement, 10% pour les tests et 10% pour la validation. Cette fonction a l'avantage de mélanger nos données.

Les données d'entraînement et de test seront utilisées pour fit le modèle. Les données de validation seront utilisées
pour tester nos prédictions.

## Notre modèle

Le modèle ci-dessous est assez simple mais est très performant. Nous nous sommes inspirés du TP de MNIST pour le
définir.

Nous avons enlevé les **Pooling** pusiqu'il est important de garder toutes les colonnes et toutes les lignes de notre
plateau (à la différence d'une image où on peut se permettre de perdre quelques pixels sur les bords).

Nous avons ajouté du **Dropout** entre les couches **Dense** pour éviter l'oversizing. Et des **BatchNormalization**
après chaque Layer. Le **BatchNormalization** nous assure que tous les batch de plateaux ont le même poids pour notre
entraînement.

Nous avons expérimenté avec différents activateurs, relu semble être le plus performant.

Nous utilisons la **MeanAbsoluteError**, car on ne cherche pas ici à dire si oui ou non un plateau est bon, on veut son
score qui est compris entre 0 et 1. Ainsi toutes les fonctions de loss de type Categorical ne sont pas intéressantes
pour notre projet. On aurait aussi pu utilise **MSE** mais on a eu de meilleurs résultats avec la **MAE**.

Les paramètres utilisés ont été trouvés de manière empirique.

### Résultats

![](.README_images/train.png)

Avec ce modèle, nous avons une **MAE de 0.0822** après 30 epochs. Pourcentages utilisés :

- train 80%
- test 10%
- validation 10%

![](.README_images/validation.png)

La figure ci-dessus a été obtenue à partir des données de validation. Nous avons calculé la MAE entre les prédictions
sur les données de validation et les valeurs attendues.

## Sauvegarder le modèle

Nous avons la possibilité de sauvegarder notre modèle au format json, pour le réutiliser plus tard. On s'en sert
notamment pour garder les modèles performants ou les modèles avec un grand nombre d'epochs, pour ne pas avoir à les
réentrainer.

## Charger un modèle déjà entraîné

Puisque les sessions de Colab n'enregistrent pas les modèles près une fermeture de session, nous avons sauvegardé nos
modèles sur Drive. Pour les réutiser directemnt, il suffit de les importer.

## Prédictions sur les données réelles

Nous téléchargeons les données jamais observées, et avec le modèle précédemment chargé, nous faisons nos prédictions.

Les prédictions sont enregistrées dans le fichier [my_predictions.txt](my_predictions.txt)

